import * as types from './actiontypes';

/*
Action Creators
*/

export function changeUser(user) {
  return {
    type: types.SELECT_USER,
    user: user
  };
}

export function addUser(userName){
  return {
    type: types.ADD_USER,
    user: {name:userName}
  };
}
