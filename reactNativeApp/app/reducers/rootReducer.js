import * as types from '../actions/actiontypes';
//import Immutable from 'seamless-immutable';

const initialState = {
  currUser: {},
  users: [{id:1,name:'Serg'},{id:2,name:'Alex'},{id:3,name:'Adam'}]
}


//root reducer
export function listUserReducer(state = initialState, action = {}) {

  switch (action.type) {
    case types.SELECT_USER:
      return Object.assign({}, state, {
        currUser: action.user
      })
    case types.ADD_USER:
      let id = state.users[state.users.length-1].id + 1;
      action.user.id = id;
      return Object.assign({}, state, {
        users: [...state.users,action.user]
      })
    default:
      return state;
  }
}
