import React, { Component } from 'react';
import { Text } from 'react-native';
import ListContainer from '../components/ListContainer';
import * as types from '../actions/actiontypes';
import {connect} from 'react-redux';

class Screen extends Component {
  selectItem = (user) => {
    this.props.onSelectUser(user);
  };

  render() {
    return (
      <ListContainer
        users={this.props.users} selectItem={this.selectItem}
      />
    );
  }
}

export default connect(
  state=>({
    users: state.users
  }),
dispatch=>({
  onSelectUser: (selected) => {
    dispatch({type:types.SELECT_USER, user:selected});
  }
}))
(Screen);
