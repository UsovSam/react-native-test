import React, { Component } from 'react';
import { Text } from 'react-native';
import OutputContainer from '../components/OutputContainer';
import * as types from '../actions/actiontypes';
import {connect} from 'react-redux';
import {addUser} from '../actions';

class Screen extends Component {
  render() {
    return (
      <OutputContainer
        user={this.props.user}
      />
    );
  }
}

export default connect(
  state=>({
    user: state.currUser
  }),
  dispatch=>({}))
(Screen);
