import React, { Component } from 'react';
import { Text } from 'react-native';
import InputContainer from '../components/InputContainer';
import * as types from '../actions/actiontypes';
import {connect} from 'react-redux';
import {addUser} from '../actions';

class Screen extends Component {
  render() {
    return (
      <InputContainer
        onClickBtn={this.props.onAddUser}
      />
    );
  }
}

export default connect(
  state=>({}),
  dispatch=>({
    onAddUser: (newName) => {
      dispatch(addUser(newName));
    }
  }))
(Screen);
