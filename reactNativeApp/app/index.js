import { Navigation } from 'react-native-navigation';
import {createStore} from 'redux';
import {Provider} from "react-redux";

import {listUserReducer} from './reducers';
const store = createStore(listUserReducer);

import Screen1 from './screens/Screen1';
import Screen2 from './screens/Screen2';
import Screen3 from './screens/Screen3';

store.subscribe(()=>{
  //console.warn("test", store.getState());
})

export default () => {
  Navigation.registerComponent('Screen1', () => Screen1, store, Provider);
  Navigation.registerComponent('Screen2', () => Screen2, store, Provider);
  Navigation.registerComponent('Screen3', () => Screen3, store, Provider);

  Navigation.startTabBasedApp({
    tabs: [
      {
        label: 'Список',
        screen: 'Screen1',
        icon: require('./images/icon1.png'),
        selectedIcon: require('./images/icon1_selected.png'),
        title: 'Список пользователей'
      },
      {
        label: 'Добавить',
        screen: 'Screen2',
        icon: require('./images/icon2.png'),
        selectedIcon: require('./images/icon2_selected.png'),
        title: 'Добавить пользователя'
      },
      {
        label: 'Информация',
        screen: 'Screen3',
        icon: require('./images/icon3.png'),
        selectedIcon: require('./images/icon3_selected.png'),
        title: 'Подробная информация'
      }
    ]
  });
};
