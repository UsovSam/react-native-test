import React from 'react';
import { View, Text,TextInput,Button, StyleSheet, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: '500',
  },
});

class InputContainer extends React.Component{

  constructor(props){
    super(props);
    this.state = {userName:""};
  }

  render() {
    return (
      <View style={[styles.container]}>
      <TextInput
          style={{height: 40, width:150, borderColor: 'gray', borderWidth: 1, margin:10}}
          onChangeText={(userName) => this.setState({userName})}
          value={this.state.userName}
        />
        <Button
          title="Добавить пользователя"
          accessibilityLabel="Добавить нового пользователя"
          onPress={()=>{
            this.props.onClickBtn(this.state.userName);
            this.setState({userName:""});
          }}
        />
      </View>
    )
  }
}

export default InputContainer;
