import React from 'react';
import { View, Text,TextInput,Button, StyleSheet, TouchableOpacity } from 'react-native';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    fontWeight: '500',
  },
});

class OutputContainer extends React.Component{

  constructor(props){
    super(props);
  }

  render() {
    return (
      <View style={[styles.container]}>
        <Text>
          {this.props.user.id}
        </Text>
        <Text>
          {this.props.user.name}
        </Text>
      </View>
    )
  }
}

export default OutputContainer;
