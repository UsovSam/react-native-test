import React from 'react';
import { View,FlatList, Text, StyleSheet, TouchableOpacity } from 'react-native';


const styles = StyleSheet.create({
  container: {
   flex: 1,
   paddingTop: 22
  },
  item: {
    padding: 10,
    fontSize: 24,
    height: 48,
  }
})

class MyListItem extends React.Component {
  onPress = () => {
    this.props.onPressItem(this.props.user);
  };

  render() {
    return (
      <TouchableOpacity onPress={this.onPress}>
        <View>
          <Text style={[styles.item]}>
            {this.props.title}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}


class ListContainer extends React.Component {
  //state = {selected: {}};

  keyExtractor = (item, index) => item.id;

  onPressItem = (item) => {
    this.props.selectItem(item);
  };

  renderItem = ({item}) => (
    <MyListItem
      key={item.id}
      user={item}
      onPressItem={this.onPressItem}
      title={item.name}
    />
  );

  render() {
    return (
      <FlatList
        data={this.props.users}
        extraData={this.state}
        keyExtractor={this.keyExtractor}
        renderItem={this.renderItem}
      />
    );
  }
}


export default ListContainer;
